import React from 'react';
import "./Sidebar.css";
import {IconButton} from "@material-ui/core";
import ChatIcon from "@material-ui/icons/Chat"
import {SearchOutlined} from "@material-ui/icons"
import SidebarChat from './SidebarChat';

function Sidebar() {
    return (
        <div className='sidebar'>
            <div className="sidebar__header">
                <div className="sidebar__headerRight">
                    <ChatIcon/> <h2>Messages</h2>
                </div>
            </div>
            <div className="sidebar__search">
                <div className="sidebar__searchContainer">
                    <SearchOutlined />
                    <input placeholder="Start new chat" type="text" />
                </div>
            </div>
            <div className="sidebar__chats">
                <SidebarChat />
                <SidebarChat />
            </div>
        </div>
    )
}

export default Sidebar