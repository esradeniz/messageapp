import React from 'react';
import './SidebarChat.css';

function SidebarChat() {
    return (
        <div className="sidebarChat">
            <div className="sidebarChat__info">
                <h4>Person Name</h4>
                <p>The last message</p>
            </div>
        </div>
    )
}

export default SidebarChat
