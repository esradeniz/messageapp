import React from 'react'
import "./Chat.css"
import BlockIcon from '@material-ui/icons/Block';
import {IconButton} from "@material-ui/core";

function Chat() {
    return (
        <div className="chat">
            <div className="chat__header">
                <h3>Person Name</h3>
                <div className="chat__block">
                    <IconButton>
                        <BlockIcon/>
                    </IconButton>
                </div>
            </div>

            <div className="chat__body">
                <p className="chat__message">
                    message 1 :) 
                    <span className="chat__timestamp">
                        {new Date().toUTCString()}
                    </span>
                </p>

                <p className="chat__message chat__receiver">
                    message 1 :) 
                    <span className="chat__timestamp">
                        {new Date().toUTCString()}
                    </span>
                </p>
            </div>

            <div className="chat__writing">
                <form>
                    <input placeholder="Write message..." type="text" />
                    <button  type="submit">
                        Send
                    </button>
                </form>
            </div>

            
        </div>
    )
}

export default Chat
